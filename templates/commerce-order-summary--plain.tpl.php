<?php foreach ($line_items as $line_item_type => $rows): ?>
  <?php foreach ($rows as $row): ?>
    <?php
    if ($row['quantity'] != 1) {
      print $row['quantity'] . ' × ';
    }
    ?>
    <?php print $row['title'] . "\t\t"; ?>
    <?php print $row['total'] . "\n"; ?>
  <?php endforeach; ?>
<?php endforeach; ?>
<?php foreach ($sub_totals as $line_item_type => $row): ?>
  <?php print '---------------------' . "\n"; ?>
  <?php print t('Subtotals:'); ?>
  <?php print $row['title'] . "\t\t"; ?>
  <?php print $row['total'] . "\n"; ?>
<?php endforeach; ?>
<?php print '---------------------' . "\n"; ?>
<?php print $order_total['title'] . "\t\t"; ?>
<?php print $order_total['total'] . "\n"; ?>
