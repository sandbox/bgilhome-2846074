<div class="order-summary">
  <div class="order-summary__line-items">
    <?php foreach ($line_items as $line_item_type => $rows): ?>
      <div class="order-summary__line-items--<?php print $line_item_type; ?>">
        <?php foreach ($rows as $row): ?>
          <div class="order-summary__line-item">
            <div class="order-summary__line-item__title">
              <?php
              if ($row['quantity'] != 1) {
                print $row['quantity'] . ' × ';
              }
              ?>
              <?php print $row['title']; ?>
            </div>
            <div class="order-summary__line-item__total">
              <?php print $row['total']; ?>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    <?php endforeach; ?>
  </div>

  <div class="order-summary__sub-totals">
    <?php foreach ($sub_totals as $line_item_type => $row): ?>
      <div class="order-summary__sub-total--<?php print $line_item_type; ?>">
        <div class="order-summary__sub-total__title">
          <?php print $row['title']; ?>
        </div>
        <div class="order-summary__sub-total__total">
          <?php print $row['total']; ?>
        </div>
      </div>
    <?php endforeach; ?>
  </div>

  <div class="order-summary__order-total">
    <div class="order-summary__order-total__title">
      <?php print $order_total['title']; ?>
    </div>
    <div class="order-summary__order-total__total">
      <?php print $order_total['total']; ?>
    </div>
  </div>
</div>
