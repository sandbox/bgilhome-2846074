<?php

/**
 * Implements hook_token_info_alter().
 */
function commerce_order_summary_token_info_alter(&$data) {
  $data['tokens']['commerce-order']['order-summary'] = array(
    'name' => t('Order summary'),
    'description' => t('Itemised line items and total for the order (HTML divs)'),
  );
  $data['tokens']['commerce-order']['order-summary-plain'] = array(
    'name' => t('Order summary - plain'),
    'description' => t('Itemised line items and total for the order (plain text)'),
  );
  // Fix commerce 'bug'/'feature' - token names using _ instead of -
  if (!isset($data['tokens']['commerce-order']['commerce-order-total'])
    && isset($data['tokens']['commerce-order']['commerce_order_total'])
  ) {
    $data['tokens']['commerce-order']['commerce-order-total'] = $data['tokens']['commerce-order']['commerce_order_total'];
  }
}

/**
 * Implements hook_tokens_alter().
 */
function commerce_order_summary_tokens_alter(array &$replacements, array $context) {
  $tokens =& $context['tokens'];
  if ($context['type'] == 'commerce-order' && !empty($context['data']['commerce_order'])) {
    $order = $context['data']['commerce_order'];
    if (isset($tokens['order-summary'])) {
      $replacements[$tokens['order-summary']] = theme('commerce_order_summary__token', array('order' => $order));
    }

    if (isset($tokens['order-summary-plain'])) {
      $replacements[$tokens['order-summary-plain']] = theme('commerce_order_summary__plain', array('order' => $order));
    }

    $preprocess_tokens = array('commerce-line-items-summary', 'commerce-order-total-tax', 'commerce-order-total-exc-tax');
    if (!empty(array_intersect_key(array_flip($preprocess_tokens), $tokens))) {
      $variables = array('order' => $order);
      $hook = 'commerce_order_summary';
      foreach (array_merge(array('template'), module_implements($hook)) as $module) {
        $fn = $module . '_preprocess_' . $hook;
        $fn($variables);
      }
    }

    if (isset($tokens['commerce-line-items-summary'])) {
      $line_item_summary = '';
      foreach ($variables['line_items'] as $product_type => $product_type_line_items) {
        foreach ($product_type_line_items as $product_type_line_item) {
          $line_item_summary .= t('@count x @product', array('@count' => number_format($product_type_line_item['quantity'], 0), '@product' => $product_type_line_item['title'])) . '<br />';
        }
      }
      $replacements[$tokens['commerce-line-items-summary']] = $line_item_summary;
    }

    if (isset($tokens['commerce-order-total-exc-tax'])) {
      $replacements[$tokens['commerce-order-total-exc-tax']] = $variables['order_total_excl_tax']['total'];
    }
    if (isset($tokens['commerce-order-total-tax'])) {
      $replacements[$tokens['commerce-order-total-tax']] = $variables['order_total_tax']['total'];
    }

    if (isset($tokens['commerce-order-total'])) {
      $total =& $order->commerce_order_total[LANGUAGE_NONE][0];
      $replacements[$tokens['commerce-order-total']] = commerce_currency_format($total['amount'], $total['currency_code']);
    }
  }
}
